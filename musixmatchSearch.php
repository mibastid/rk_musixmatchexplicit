<?php

$firstSongToSearch= 0; //comienza en 0
$numSongsToSearch = 10;

if(count($argv)==3){
  $firstSongToSearch= $argv[1]; //comienza en 0
  $numSongsToSearch = $argv[2];
}elseif (count($argv)==2) {
  $firstSongToSearch= $argv[1]; //comienza en 0
  $numSongsToSearch = 100;
}



$db = connectDB();
$songsToSearch = getSongsBD($db, $firstSongToSearch, $numSongsToSearch);

// printSomething('$songsToSearch');
// printSomething($songsToSearch);

$fileJson = file_get_contents("genres.json");
$jsonGenres = json_decode($fileJson, true);

$genreNamesIncluded = [];
foreach ($jsonGenres as $item) {
  array_push($genreNamesIncluded, $item['name']);
}
$numInitialGenres = count($genreNamesIncluded);

$fileJson = file_get_contents("songGenres.json");
$jsonSongGenres = json_decode($fileJson, true);

$idSongsIncluded =[];
foreach ($jsonSongGenres as $item) {
  array_push($idSongsIncluded, $item['idSong']);
}

$counter = 0;
$counterFound = 0;

foreach ($songsToSearch as $currentSong) {
  $idSong = $currentSong['idSong'];
  $artist = $currentSong['artist'];
  $song = $currentSong['song'];

  if(!in_array($idSong, $idSongsIncluded)){
    $url = "http://api.musixmatch.com/ws/1.1/track.search?apikey=d6263d53675832ae7ff64e30871c0d2b&q_artist=".urlencode($artist)."&q_track=".urlencode($song);

    $response = file_get_contents($url);
    $response = json_decode($response, true);

    $tracks = $response['message']['body']['track_list'];

    foreach ($tracks as $track) {
      if(mb_strtoupper(trim($track['track']['track_name']), 'UTF-8') == mb_strtoupper(trim($song), 'UTF-8') && mb_strtoupper(trim($track['track']['artist_name']), 'UTF-8') == mb_strtoupper(trim($artist), 'UTF-8')){
        $explicit = $track['track']['explicit'];
        $genres = [];
        foreach ($track['track']['primary_genres']['music_genre_list'] as $genre) {
          array_push($genres, $genre['music_genre']['music_genre_name']);
        }

        foreach ($track['track']['secondary_genres']['music_genre_list'] as $genre) {
          array_push($genres, $genre['music_genre']['music_genre_name']);
        }
        // este vector será el que guarda lós géneros de la canción actual
        $currentPosGenres = [];
        foreach ($genres as $genre) {
          if(!in_array($genre, $genreNamesIncluded)){
            array_push($currentPosGenres, count($jsonGenres));
            array_push($jsonGenres, [
              'pos' => count($jsonGenres),
              'name' => $genre,
              'idGenreRK' => getIdGenre($db, $genre),
              'idFather' => -1
              ]);
            array_push($genreNamesIncluded, $genre);
          } else{
            array_push($currentPosGenres, searchPosGenre($jsonGenres, $genre));
          }
        }

        array_push($jsonSongGenres, [
          'idSong' => $idSong,
          'posGenre' => $currentPosGenres,
          'explicit' => $explicit
          ]);

        array_push($idSongsIncluded, $idSong);

        $counterFound ++;
        break;
      }
    }
  }
  $counter ++;
  printSomething($counter . '(' . $counterFound . ') - ' . $song . ' - ' . $artist  . ' (db: '. ($firstSongToSearch + $counter -1) . ')');
}

// foreach ($jsonGenres as $genre) {
//   printSomething($genre['name']);
// }
// printSomething('$jsonGenres');
// printSomething($jsonGenres);
// printSomething('$jsonSongGenres');
// printSomething($jsonSongGenres);
printSomething('Número de géneros incluidos en esta tanda: '. (count($jsonGenres) - $numInitialGenres));
printSomething('Número de canciones encontradas en esta tanda: '.$counterFound . ' de '. $counter);

$fp = fopen('genres.json', 'w');
fwrite($fp, json_encode($jsonGenres));
fclose($fp);
$fp = fopen('songGenres.json', 'w');
fwrite($fp, json_encode($jsonSongGenres));
fclose($fp);


closeDB($db);



return;

/*********************************
ESTO ES PARA GUARDARLO TODO EN BD
*********************************/
//no hace falta si no se cierra antes
$db = connectDB();

$jsonGenresLen = count($jsonGenres);
for ($i=0; $i < $jsonGenresLen; $i++) { 
  if($jsonGenres['idGenreRK']==0){
    $jsonGenres['idGenreRK'] = saveNewGenre($db, $jsonGenres['name']);
  }
}

foreach ($jsonSongGenres as $item) {
  if(count($item['posGenre'])>0){
    $itemGenres = [];
    foreach ($item['posGenre'] as $posGenre) {
      if($jsonGenres[$posGenre]['idGenreRK'] != 0){
        array_push($itemGenres, $jsonGenres[$posGenre]['idGenreRK']);
      }
    }
    saveSongGenres($db, $item['idSong'], $itemGenres);
  }
  saveSongExplicit($db, $item['idSong'], $item['explicit']);
}
$jsonSongGenres = []; //lo vaciamos porque estas canciones las acabamos de guardar en BD
//jsonGenres no lo borramos porque nos interesa ir acumulándolo

//quitar estas mismas líneas de más arriba
$fp = fopen('genres.json', 'w');
fwrite($fp, json_encode($jsonGenres));
fclose($fp);
$fp = fopen('songGenres.json', 'w');
fwrite($fp, json_encode($jsonSongGenres));
fclose($fp);

closeDB($db);















function connectDB(){
  $dbhost="preprod.redkaraoke.es";  
  $dbuser="redkaraoke"; 
  $dbpassword="gizmo"; 
  $db="redkaraoke";        

  $database = new mysqli($dbhost, $dbuser, $dbpassword, $db);
  if ($database->connect_errno) {
    echo "Failed to connect to MySQL: (" . $database->connect_errno . ") " . $database->connect_error;
  }
  $database->set_charset('utf8');
  return $database;
}

function closeDB($db){
  mysqli_close($db);
}


function getSongsBD($db, $start, $size){
  $songs = [];

  $query = "SELECT s.SON_ID_SONG, s.SON_TITLE, a.ART_NAME FROM SONG AS s, ARTIST AS a WHERE s.ART_ID_ARTIST = a.ART_ID_ARTIST ORDER BY s.SON_ID_SONG LIMIT ". $db->real_escape_string($start) . ','.$db->real_escape_string($size);
  $result = $db->query($query);
  while ($row = $result->fetch_object()) {
    array_push($songs, ['song' => $row->SON_TITLE, 'artist' => $row->ART_NAME, 'idSong' => $row->SON_ID_SONG]);
  }
  
  return $songs;
}


function searchPosGenre($genres, $search){
  foreach ($genres as $genre) {
    if($genre['name'] == $search){
      return $genre['pos'];
    }
  }
  return -1; //siempre lo va a encontrar
}



function getIdGenre($db, $genre){
  $query = "SELECT GEN_ID_GENRE FROM GENRE WHERE GEN_NAME = '" . $db->real_escape_string($genre) . "' LIMIT 1";
  $result = $db->query($query);
  if($result->num_rows > 0){
    return intval($result->fetch_object()->GEN_ID_GENRE);
  } else{
    return 0;
  }
}

function saveNewGenre($db, $genre){
 $query = "INSERT INTO GENRE (GEN_NAME) VALUES ('".$db->real_escape_string($genre)."')";
 $result = $db->query($query);
 //no estoy seguro de que vaya a devolver el id que queremos
 return mysqli_insert_id($db);
}

function saveSongGenres($db, $idSong, $genresId){

}

function saveSongExplicit($db, $idSong, $explicit){

}








function printSomething($thing){
  $inConsole = true;

  if(!$inConsole){
    echo '<hr style="border-top: 2px solid green;">';
  }
  
  if(is_array($thing)){
    var_dump($thing);
  }else{
    echo $thing;
  }

  if(!$inConsole){
    echo '<hr style="border-top: 2px solid red;">';
  }else{
    echo "\r\n";
  }
  
}

